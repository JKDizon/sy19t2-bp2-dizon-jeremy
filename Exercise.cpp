#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <time.h> 

using namespace std;

struct Item
{
	string name;
	int goldValue;
};

void getRandomItem(Item* soldItems, int count)
{
	Item storage[] = { { "Potion", 10 },
						 { "Mega Potion", 20 },
						 { "Max Potion", 35 },
						 { "Ancient Potion", 30 },
						 { "Antidote", 10 },
						 { "Lifepowder", 25 },
						 { "Energy Drink", 20 },
						 { "Mega Demondrug", 30 },
						 { "Mega Armorskin", 30 },
						 { "Dash Drink", 25 },
						 { "Cool Drink", 20 },
						 { "Hot Drink", 20 },
						 { "Whetstone", 15 },
						 { "Farcaster", 25 },
						 { "Trap Tool", 25 },
						 { "Shock Trap", 30 },
						 { "Pitfall Trap", 25 },
						 { "Paintball", 15 },
						 { "Flashbomb", 15 },
						 { "Catalyst", 20 } };

	int x = count;
	int y = rand() % 20 + 1;

	soldItems[x].name = storage[y].name;
	soldItems[x].goldValue = storage[y].goldValue;
};

void printTotalGold(Item* items)
{
	int sum = 0;

	for (int n = 0; n < 10; n++)
	{
		cout << items[n].name << "\t" << items[n].goldValue << endl;
		sum = sum + items[n].goldValue;
	}

	cout << endl << "Total Gold: " << sum;
};

int main()
{
	srand(time(NULL));

	Item soldItems[10];

	for (int n = 0; n < 10; n++)
	{
		getRandomItem(soldItems, n);
	}

	cout << "Items Bought:" << endl;
	printTotalGold(soldItems);

	system("pause");

	return 0;
}