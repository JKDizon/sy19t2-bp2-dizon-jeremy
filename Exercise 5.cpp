#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class Wizard
{
public:
	Wizard();
	Wizard(string Name, int HP, int MP);


	int CastSpell(string SpellName);

	void Attack(Wizard* otherWizard);
	void Damage(int p_damage);
	void viewstats();

	string MName;	int MHP;
	int MMP;
};


Wizard::Wizard()
{
	MName = "Blank";
	MHP = 500;
	MMP = 250;
}

Wizard::Wizard(string Name, int HP, int MP)
{
	MName = Name;
	MHP = HP;
	MMP = MP;
}

void Wizard::viewstats()
{
	cout << "Name: " << MName << endl;
	cout << "HP: " << MHP << endl;
	cout << "MP: " << MMP << endl;

	cout << endl;
}

void Wizard::Attack(Wizard* p_otherWiz)
{
	Spell Spell1("Fireball", 30, 40);
	p_otherWiz->MHP -= 40;
}

int Wizard::CastSpell(string SpellName)
{
	return 0;
}

Spell::Spell()
{
	SpellName = "Nothing";
	Damage = 0;
}


Spell::Spell(string SpellName, int MPCost, int Damage)
{

}

class Spell
{
public:
	Spell();
	Spell(string SpellName, int MPCost, int Damage);
	string SpellName;
	int MPCost;
	int Damage;
};