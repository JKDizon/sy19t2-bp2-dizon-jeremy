#include <iostream>
#include<cstdlib>
#include<time.h>

using namespace std;

void bet(int& currentGold, int& returnBet) {
	int bet = 0;
	

	cout << "What is your bet?" << endl;
	cin >> bet;
	while (bet <= 0 || bet > currentGold) {
		cout << "Wrong input." << endl;
		cin.clear();
		cin.ignore(32767, '\n');
		cin >> bet;
	}


	returnBet = bet;
	currentGold = currentGold - bet;
}

void rollDice(int rollOne[2], int rollTwo[2]) {

	rollOne[0] = rand() % 6 + 1;
	rollOne[1] = rand() % 6 + 1;

	rollTwo[0] = rand() % 6 + 1;
	rollTwo[1] = rand() % 6 + 1;

}

void payout(int playerRoll[2], int enemyRoll[2], int& modifiedGold, int returnBet) {
	int sumPlayer = playerRoll[0] + playerRoll[1];
	int sumEnemy = enemyRoll[0] + enemyRoll[1];


	if ((sumPlayer == 2) && (sumPlayer == sumEnemy)) {
		cout << "It's a draw." << endl;
		modifiedGold = modifiedGold + (returnBet * 1);
	}
	else if (sumPlayer == 2) {
		modifiedGold = modifiedGold + (returnBet * 3);
	}
	else if (sumPlayer == sumEnemy) {
		cout << "It's a draw." << endl;
		modifiedGold = modifiedGold + (returnBet * 1);
	}
	else if (sumPlayer > sumEnemy) {
		cout << "You won." << endl;
		modifiedGold = modifiedGold + (returnBet * 1);
	}
	else if (sumPlayer < sumEnemy) {
		cout << "You lost." << endl;
	}

}

void playRound(int& userGoldRef) {
	int returnBet = 0;
	int playerRoll[2] = { 0,0 };
	int enemyRoll[2] = { 0,0 };


	bet(userGoldRef, returnBet);
	rollDice(playerRoll, enemyRoll);

	cout << endl;
	cout << "The player roll is: " << endl;
	for (int i = 0; i < 2; i++) {
		cout << playerRoll[i] << endl;
	}

	cout << endl;
	cout << "The enemy roll is: " << endl;
	for (int i = 0; i < 2; i++) {
		cout << enemyRoll[i] << endl;
	}

	payout(playerRoll, enemyRoll, userGoldRef, returnBet);


	cout << "Your current gold is: " << userGoldRef << endl;
	cout << "Your bet was: " << returnBet << endl;

}

int main() {
	int userGold = 1000;

	srand(time(NULL));

	while (userGold > 0) {
		playRound(userGold);
	};

	if (userGold <= 0) {
		cout << "Can't play, you broke bye." << endl;
	}

	system("pause");
}