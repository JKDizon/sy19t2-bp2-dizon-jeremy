#include <iostream>
#include <string>
#include <time.h>

using namespace std;

struct Node
{
	string name;
	Node *next;
};

struct TonegawaNode
{
	string tonegawaCard;
	TonegawaNode *next;
};

void append(struct Node** headRef, string name)
{
	struct Node* newNode = new Node;

	struct Node *last = *headRef;

	newNode->name = name;

	newNode->next = NULL;

	if (*headRef == NULL)
	{
		*headRef = newNode;
		return;
	}

	while (last->next != NULL)
		last = last->next;

	last->next = newNode;
	return;
}

void tonegawaAppend(struct TonegawaNode** headRef, string tonegawaCard)
{
	TonegawaNode* newNode = new TonegawaNode;

	TonegawaNode *last = *headRef;

	newNode->tonegawaCard = tonegawaCard;

	newNode->next = NULL;

	if (*headRef == NULL)
	{
		*headRef = newNode;
		return;
	}

	while (last->next != NULL)
		last = last->next;

	last->next = newNode;
	return;
}

void print(struct Node *node)
{
	while (node != NULL)
	{
		cout << node->name << endl;
		node = node->next;
	}
}

void deleteNode(struct Node **headRef, int position)
{
	if (*headRef == NULL)
		return;

	struct Node* temp = *headRef;

	if (position == 0)
	{
		*headRef = temp->next;    
		free(temp);               
		return;
	}

	for (int i = 0; temp != NULL && i < position - 1; i++)
		temp = temp->next;

	if (temp == NULL || temp->next == NULL)
		return;

	struct Node *next = temp->next->next;

	free(temp->next);  

	temp->next = next;  
}

void tonegawaDelete(struct TonegawaNode **headRef, int position)
{
	if (*headRef == NULL)
		return;

	TonegawaNode* temp = *headRef;

	if (position == 0)
	{
		*headRef = temp->next;
		free(temp);
		return;
	}

	for (int i = 0; temp != NULL && i < position - 1; i++)
		temp = temp->next;

	if (temp == NULL || temp->next == NULL)
		return;

	TonegawaNode *next = temp->next->next;

	free(temp->next);

	temp->next = next;
}

void deleteList(struct Node** headRef)
{
	struct Node* current = *headRef;
	struct Node* next;

	while (current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}

	*headRef = NULL;
}

void tonegawaDeleteList(struct TonegawaNode** headRef)
{
	TonegawaNode* current = *headRef;
	TonegawaNode* next;

	while (current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}

	*headRef = NULL;
}

int main()
{
	srand(time(NULL));
	Node *head = NULL;
	TonegawaNode *tonegawaHead = NULL;
	int cash = 0;
	int distance = 30;
	int round = 1;
	int bet;
	int choice;
	int tonegawaChoice;
	int tonegawaTotal = 4;
	int value;
	int& size = value;
	int count;
	string roundController;
	string side;
	string eCard;
	string sCard;
	string organ;

	///////////

	cout << "You can choose between your 'ear' or your 'eye'" << endl;
	cout << "Organ to bet: ";
	cin >> organ;

	while (cash <= 20000000 && round < 4)
	{
		side = "Emperor";
		count = 4;
		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << distance << endl;
		cout << "Round: " << round << "/12" << endl;
		cout << "Side: " << side << endl;
		cout << endl;
		cout << "How much mm would you like to wager, Kaiji? ";
		cin >> bet;

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				eCard = "Emperor";
			}
			if (i == 1)
			{
				eCard = "Civilian";
			}
			append(&head, eCard);
		}

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				sCard = "Slave";
			}
			if (i == 1)
			{
				sCard = "Civilian";
			}
			tonegawaAppend(&tonegawaHead, sCard);
		}
		
		while (roundController == "Draw" || round <= 5)
		{
			cout << "Pick a card to play" << endl;
			cout << "(from top to bottom, input 0 to " << count << ")" << endl;
			print(head);
			cin >> choice;
			deleteNode(&head, choice);

			tonegawaChoice = rand() % tonegawaTotal;
			tonegawaDelete(&tonegawaHead, tonegawaChoice);

			if (choice == 0 && tonegawaChoice > 0)
			{
				roundController = "Kaiji wins";
				cout << "Kaiji wins" << endl;
				cash = cash + (bet * 100000);
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else if (choice > 0 && tonegawaChoice == 0)
			{
				roundController = "Kaiji lost";
				cout << "Kaiji lost" << endl;
				distance -= bet;
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else
			{
				roundController = "Draw";
				cout << "Draw" << endl;
				system("pause");
				system("cls");
			}
			count--;
			cout << endl;
		}
		cout << endl;



		if (cash == 20000000)
			cout << "Great Job Kaiji you got enough money" << endl;

		else if (distance == 0)
			cout << "You got your " << organ << " drilled" << endl;
	}

	while (cash <= 20000000 && round > 3 && round < 7)
	{
		side = "Slave";
		count = 4;
		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << distance << endl;
		cout << "Round: " << round << "/12" << endl;
		cout << "Side: " << side << endl;
		cout << endl;
		cout << "How much mm would you like to wager, Kaiji? ";
		cin >> bet;

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				eCard = "Slave";
			}
			if (i == 1)
			{
				eCard = "Civilian";
			}
			append(&head, eCard);
		}

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				sCard = "Emperor";
			}
			if (i == 1)
			{
				sCard = "Civilian";
			}
			tonegawaAppend(&tonegawaHead, sCard);
		}

		while (roundController == "Draw" || round <= 5)
		{
			cout << "Pick a card to play" << endl;
			cout << "(from top to bottom, input 0 to " << count << ")" << endl;
			print(head);
			cin >> choice;
			deleteNode(&head, choice);

			tonegawaChoice = rand() % tonegawaTotal;
			tonegawaDelete(&tonegawaHead, tonegawaChoice);

			if (choice == 0 && tonegawaChoice == 0)
			{
				roundController = "Kaiji wins";
				cout << "Kaiji wins" << endl;
				cash = cash + (bet * 500000);
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else if (choice == 0 && tonegawaChoice > 0)
			{
				roundController = "Kaiji lost";
				cout << "Kaiji lost" << endl;
				distance -= bet;
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else 
			{
				roundController = "Draw";
				cout << "Draw" << endl;
				system("pause");
				system("cls");
			}
			count--;
			cout << endl;
		}
		cout << endl;



		if (cash == 20000000)
			cout << "Great Job Kaiji you got enough money" << endl;

		else if (distance == 0)
			cout << "You got your " << organ << " drilled" << endl;
	}

	while (cash <= 20000000 && round > 6 && round < 10)
	{
		side = "Emperor";
		count = 4;
		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << distance << endl;
		cout << "Round: " << round << "/12" << endl;
		cout << "Side: " << side << endl;
		cout << endl;
		cout << "How much mm would you like to wager, Kaiji? ";
		cin >> bet;

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				eCard = "Emperor";
			}
			if (i == 1)
			{
				eCard = "Civilian";
			}
			append(&head, eCard);
		}

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				sCard = "Slave";
			}
			if (i == 1)
			{
				sCard = "Civilian";
			}
			tonegawaAppend(&tonegawaHead, sCard);
		}

		while (roundController == "Draw" || round <= 5)
		{
			cout << "Pick a card to play" << endl;
			cout << "(from top to bottom, input 0 to " << count << ")" << endl;
			print(head);
			cin >> choice;
			deleteNode(&head, choice);

			tonegawaChoice = rand() % tonegawaTotal;
			tonegawaDelete(&tonegawaHead, tonegawaChoice);

			if (choice == 0 && tonegawaChoice > 0)
			{
				roundController = "Kaiji wins";
				cout << "Kaiji wins" << endl;
				cash = cash + (bet * 100000);
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else if (choice > 0 && tonegawaChoice == 0)
			{
				roundController = "Kaiji lost";
				cout << "Kaiji lost" << endl;
				distance -= bet;
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else
			{
				roundController = "Draw";
				cout << "Draw" << endl;
				system("pause");
				system("cls");
			}
			count--;
			cout << endl;
		}
		cout << endl;



		if (cash == 20000000)
			cout << "Great Job Kaiji you got enough money" << endl;

		else if (distance == 0)
			cout << "You got your " << organ << " drilled" << endl;
	}

	while (cash <= 20000000 && round > 9 && round < 13)
	{
		side = "Slave";
		count = 4;
		cout << "Cash: " << cash << endl;
		cout << "Distance left (mm): " << distance << endl;
		cout << "Round: " << round << "/12" << endl;
		cout << "Side: " << side << endl;
		cout << endl;
		cout << "How much mm would you like to wager, Kaiji? ";
		cin >> bet;

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				eCard = "Slave";
			}
			if (i == 1)
			{
				eCard = "Civilian";
			}
			append(&head, eCard);
		}

		for (int i = 0; i <= 4; i++)
		{
			if (i == 0)
			{
				sCard = "Emperor";
			}
			if (i == 1)
			{
				sCard = "Civilian";
			}
			tonegawaAppend(&tonegawaHead, sCard);
		}

		while (roundController == "Draw" || round <= 5)
		{
			cout << "Pick a card to play" << endl;
			cout << "(from top to bottom, input 0 to " << count << ")" << endl;
			print(head);
			cin >> choice;
			deleteNode(&head, choice);

			tonegawaChoice = rand() % tonegawaTotal;
			tonegawaDelete(&tonegawaHead, tonegawaChoice);

			if (choice == 0 && tonegawaChoice == 0)
			{
				roundController = "Kaiji wins";
				cout << "Kaiji wins" << endl;
				cash = cash + (bet * 500000);
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else if (choice == 0 && tonegawaChoice > 0)
			{
				roundController = "Kaiji lost";
				cout << "Kaiji lost" << endl;
				distance -= bet;
				deleteList(&head);
				tonegawaDeleteList(&tonegawaHead);

				round++;
				system("pause");
				system("cls");
				break;
			}

			else
			{
				roundController = "Draw";
				cout << "Draw" << endl;
				system("pause");
				system("cls");
			}


			count--;
			cout << endl;
		}
		cout << endl;



		if (cash == 20000000)
			cout << "Great Job Kaiji you got enough money" << endl;

		else if (round >= 13)
			cout << "Sorry Kaiji, you didn't get enough money" << endl;

		else if (distance == 0)
			cout << "You got your " << organ << " drilled" << endl;
	}

	system("cls");
	system("pause");
}