#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
//#include "Node.h"

#ifndef NODE_H
#define NODE_H

#include <string>

struct Node
{
    std::string name;
    Node* next = NULL;
    Node* previous = NULL;
};

#endif //NODE_H

using namespace std;

struct Node* nightsWatch = (struct Node*)malloc(sizeof(struct Node));

void insert(string newName) {
    struct Node* temp = new Node;
    temp->name = newName;
    temp->next = nightsWatch;
    nightsWatch = temp;
}

void display() {
    struct Node* temp = nightsWatch;

    while (temp != NULL) {
        cout << temp->name << endl;
        temp = temp->next;
    }
    cout << endl;
}

int main() {
    srand(time(NULL));

    char choice;
    string name;

    cout << "================================ THE WALL ================================" << endl << endl
        << "The Night�s Watch was overwhelmed by the White Walkers. The Night�s Watch" << endl
        << "is not enough to defend the Wall. If the White Walkers get past through " << endl
        << "the Wall, it will be over for the Seven Kingdoms." << endl << endl
        << "The Wall will only last for another 3 days so the Night�s Watch decided " << endl
        << "to seek help from the Seven Kingdoms. However, they can only afford to " << endl
        << "send one member to seek help. Who will the Night�s Watch send?" << endl << endl
        << "==========================================================================" << endl;

    do {
        cout << endl << "What's your name soldier? ";
        cin >> name;
        insert(name);
        cout << endl << "Are there more soldiers (Y/N)? ";
        cin >> choice;
    } while (choice != 'N' && choice != 'n');

    // clrscr();

    cout << "==========================================" << endl
        << "ROUND 1" << endl
        << "==========================================" << endl << endl
        << "Remaining Members:" << endl;

    display();

    cout << "Result: " << endl;

    return 0;
}